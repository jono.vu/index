import React from "react"

import { content } from "../utils/useContent.js"
import Markdown from "markdown-to-jsx"
import FolioSrc from "../images/folio.pdf"

const Intro = () => {
  const intro = content.intro
  return <Markdown>{intro}</Markdown>
}

const Folio = () => {
  return (
    <>
      Folio available:{" "}
      <a target="_blank" href={FolioSrc}>
        here
      </a>
    </>
  )
}

const Projects = () => {
  const projects = content.projects
  return (
    <ul>
      {projects.map((project, i) => {
        return (
          <li key={i}>
            <a href={project.url} target="_blank">
              {project.title}
            </a>
            <span> {project?.date}</span>
          </li>
        )
      })}
    </ul>
  )
}

const Landing = () => (
  <>
    <Intro />
    <Folio />
    <Projects />
  </>
)

export default Landing
