export const content = {
  intro: `Hi I am Jonathan. Welcome!`,
  projects: [
    {
      title: `Mount Film`,
      url: `https://mount-film.netlify.app`,
      // date: `15 Feb 2020`,
    },
    {
      title: `Pong! Controls: P1: W/S P2: Up-Arrow/Down-Arrow`,
      url: `https://jon-pong.netlify.app`,
      // date: `1 Feb 2020`,
    },
    {
      title: `Chord Name`,
      url: `https://chord.name`,
    },
    {
      title: `Riverside Creative`,
      url: `https://riverside-creative.netlify.app`,
    },
    {
      title: `Renov Project`,
      url: `https://renov-project.netlify.app`,
    },
  ],
}
