import React from "react"

// import Landing from "../components/Landing"

import { navigate } from "@reach/router"
import FolioPdf from "../images/folio.pdf"

const Home = () => {
  if (typeof window === `undefined`) return null
  window.location.href = FolioPdf
  return null
}

export default Home
